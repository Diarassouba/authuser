package com.testTechnique.user.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "utilisateur")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique=true, nullable = false)
    private Long id;

    @Column(name="name")
    @NotNull
    private String name;

    @Column(name = "date_naissance")
    @NotNull
    private LocalDate dateNaissance;

    @ManyToOne
    @JsonIgnoreProperties("utilisateur")
    @NotNull
    private Pays pays;

    @Size(max = 20)
    @Column(name = "tel_portable", length = 150)
    private String telPortable;

    @Size(max = 10)
    private String sexe;

}
