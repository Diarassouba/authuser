package com.testTechnique.user.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Pays.
 *
 * @author
 */
@Entity
@Table(name = "pays")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pays implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "code", length = 50)
    private String code;

    @Column(name = "age_majorite")
    private Integer ageMajorite;

    @Column(name = "nom", length = 150)
    private String nom;

}
