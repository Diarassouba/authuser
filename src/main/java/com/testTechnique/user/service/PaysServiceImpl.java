package com.testTechnique.user.service;

import com.testTechnique.user.domain.Pays;
import com.testTechnique.user.repository.PaysRepository;
import com.testTechnique.user.service.dto.PaysDTO;
import com.testTechnique.user.service.error.EntityNotFoundException;
import com.testTechnique.user.service.mapper.PaysMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class PaysServiceImpl implements  PaysService{

    private final PaysMapper paysMapper;

    private final PaysRepository paysRepository;

    private final Logger log = LoggerFactory.getLogger(PaysServiceImpl.class);

    public PaysServiceImpl(PaysMapper paysMapper, PaysRepository paysRepository) {
        this.paysMapper = paysMapper;
        this.paysRepository = paysRepository;
    }

    /**
     * Save a pays.
     *
     * @param paysDTO the entity to save.
     * @return the persisted entity.
     */
    public PaysDTO save(PaysDTO paysDTO) {
       log.debug("Request to save Pays : {}", paysDTO);
        Pays pays = paysMapper.toEntity(paysDTO);
        pays = paysRepository.save(pays);
        return paysMapper.toDto(pays);
    }


    /**
     * Supprime le pays à partir de son id.
     *
     * @param id l'id de l'entité.
     */
    @Override
    public Boolean delete(Long id) {
        log.debug("Requête pour supprimer le pays : {}", id);
        try {
            paysRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException("Ce pays n'existe pas "+ false);
        }
    }

    @Override
    public List<PaysDTO> findAll() {
        log.debug("Request to get all Adresses");
        return paysRepository.findAll().stream().map(paysMapper::toDto).collect(Collectors.toList());
    }

    /**
     * Get one pays by id.
     *
     * @param id l'id de l'entité.
     * @return the entity.
     */
    public PaysDTO findOne(Long id) {
        log.debug("Request to get Pays : {}", id);
      Optional<Pays> pays =  paysRepository.findById(id);
        if (!pays.isPresent()){
            throw new EntityNotFoundException(" Ce pays n'exsite pas "+id);
        }
        return paysMapper.toDto(pays.get());
    }
}
