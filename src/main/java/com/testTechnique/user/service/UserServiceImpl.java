package com.testTechnique.user.service;

import com.testTechnique.user.service.dto.ResponseUserDTO;
import com.testTechnique.user.service.error.EntityNotFoundException;
import com.testTechnique.user.domain.Pays;
import com.testTechnique.user.domain.User;
import com.testTechnique.user.repository.PaysRepository;
import com.testTechnique.user.repository.UserRepository;
import com.testTechnique.user.service.dto.UserDTO;
import com.testTechnique.user.service.error.InvalidEntityException;
import com.testTechnique.user.service.mapper.ResponseMapper;
import com.testTechnique.user.service.mapper.UserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;

    private final ResponseMapper responseMapper;

    private final PaysRepository paysRepository;

    private final UserRepository userRepository;

    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
   private final static String  PAYS_EXCLU ="France";


    public UserServiceImpl(UserMapper userMapper, ResponseMapper responseMapper, PaysRepository paysRepository, UserRepository userRepository) {
        this.userMapper = userMapper;
        this.responseMapper = responseMapper;
        this.paysRepository = paysRepository;
        this.userRepository = userRepository;
    }

    /**
     * Save a user.
     *
     * @param userDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ResponseUserDTO save(UserDTO userDTO) {
        log.debug("Request to save user : {}", userDTO);
      Optional<Pays> pays =  paysRepository.findById(userDTO.getPays().getId());
      if (!pays.isPresent()){
          throw new EntityNotFoundException("Le pays don l'id est "+userDTO.getPays().getId()+" n'existe pas");
      }else if (pays.get().getNom().equalsIgnoreCase(PAYS_EXCLU)
                && Period.between(userDTO.getDateNaissance(), LocalDate.now()).getYears() < pays.get().getAgeMajorite()){
            throw new InvalidEntityException("La France n'accepte pas les minieurs ");
        }
        User user = userMapper.toEntity(userDTO);
        user = userRepository.save(user);
        return responseMapper.toDto(user);
    }

    /**
     *
     * @param id user
     * @return UserDTO
     */
    @Override
    public UserDTO findById(Long id) {
      Optional<User> user =  userRepository.findById(id);
      if (!user.isPresent()){
          throw new EntityNotFoundException("L'utilisateur n'existe pas ");
      }
        return  userMapper.toDto(user.get()) ;
    }

    /**
     * retourne l'ensemble des user.
     *
     * @return the list of entities.
     */
    @Override
    public List<UserDTO> findAll() {
        log.debug("Request to get all Adresses");

        return userRepository.findAll().stream().map(userMapper::toDto).collect(Collectors.toList());
    }

}
