package com.testTechnique.user.service.error;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class EntityNotFoundException extends RuntimeException{

  private String message;
  private HttpStatus httpStatus;

    public  EntityNotFoundException(String message){
    super();
    this.message = message;
    this.httpStatus = HttpStatus.NOT_FOUND;
    }
}
