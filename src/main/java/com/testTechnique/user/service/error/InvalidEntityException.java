package com.testTechnique.user.service.error;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class InvalidEntityException extends RuntimeException {

    private String message;
    private HttpStatus httpStatus;

    public InvalidEntityException(String message) {
        super();
        this.message = message;
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }
}