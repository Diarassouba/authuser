package com.testTechnique.user.service;

import com.testTechnique.user.service.dto.PaysDTO;

import java.util.List;

public interface PaysService {

    PaysDTO save(PaysDTO paysDTO);
    Boolean delete(Long id);

    List<PaysDTO> findAll();

   PaysDTO findOne(Long id);
}
