package com.testTechnique.user.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaysDTO {

    private Long id;

    private String code;

    private Integer ageMajorite;

    private String nom;

}
