package com.testTechnique.user.service.dto;

import com.sun.istack.NotNull;
import lombok.Data;

import java.time.LocalDate;

@Data
public class ResponseUserDTO {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private LocalDate dateNaissance;

    @NotNull
    private Long paysId;

    private String telPortable;

    private String sexe;
}
