package com.testTechnique.user.service.dto;

import com.sun.istack.NotNull;
import com.testTechnique.user.domain.Pays;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private LocalDate dateNaissance;

    @NotNull
    private Pays pays;

    private String telPortable;

    private String sexe;
}
