package com.testTechnique.user.service;

import com.testTechnique.user.service.dto.ResponseUserDTO;
import com.testTechnique.user.service.dto.UserDTO;

import java.util.List;

public interface UserService {

    ResponseUserDTO save(UserDTO userDTO);
    UserDTO findById(Long id);
    List<UserDTO> findAll();
}
