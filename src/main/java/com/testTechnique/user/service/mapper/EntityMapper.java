package com.testTechnique.user.service.mapper;

import java.util.List;

/**
 * Contrat générique entre le mapper et le DTO.
 *
 * @param <D> - le DTO.
 * @param <E> - l'entité.
 */

public interface EntityMapper<D, E> {

    E toEntity(D dto);

    D toDto(E entity);

    List<E> toEntity(List<D> dtoList);

    List<D> toDto(List<E> entityList);
}
