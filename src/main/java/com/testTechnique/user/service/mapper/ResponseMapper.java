package com.testTechnique.user.service.mapper;

import com.testTechnique.user.domain.User;
import com.testTechnique.user.service.dto.ResponseUserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {PaysMapper.class})
public interface ResponseMapper extends EntityMapper<ResponseUserDTO, User>{

    @Mapping(source = "pays.id", target = "paysId")
    ResponseUserDTO toDto(User user);

    default User fromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }
}
