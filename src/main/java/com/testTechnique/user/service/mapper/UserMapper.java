package com.testTechnique.user.service.mapper;

import com.testTechnique.user.domain.User;
import com.testTechnique.user.service.dto.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper(componentModel = "spring", uses = {PaysMapper.class})
public interface UserMapper extends EntityMapper<UserDTO, User>{
    @Override
    @Mapping(source = "pays", target = "pays")
    UserDTO toDto(User user);

    @Override
    User toEntity(UserDTO userDTO);

    default User fromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }

}
