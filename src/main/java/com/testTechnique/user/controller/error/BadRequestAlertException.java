package com.testTechnique.user.controller.error;

import lombok.Data;
import org.springframework.http.HttpStatus;


@Data
public class BadRequestAlertException extends RuntimeException{

    private String message;
    private HttpStatus httpStatus;

    public BadRequestAlertException(String message) {

        super();
        this.message = message;
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }
}
