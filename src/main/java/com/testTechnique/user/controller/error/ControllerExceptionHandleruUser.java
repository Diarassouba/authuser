package com.testTechnique.user.controller.error;

import com.testTechnique.user.service.error.EntityNotFoundException;
import com.testTechnique.user.service.error.InvalidEntityException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.zalando.problem.Status;

import java.util.Date;


@RestControllerAdvice
public class ControllerExceptionHandleruUser {
    @ExceptionHandler(value = {EntityNotFoundException.class})
    ResponseEntity<Object> entityNotFoundException(EntityNotFoundException ex){
        ErrorMessage errorManager = ErrorMessage.builder()
                .message(ex.getMessage())
                .timestamp(new Date())
                .code(Status.NOT_FOUND.getStatusCode())
                .build();
        return new ResponseEntity<>(errorManager, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {InvalidEntityException.class})
    ResponseEntity<Object> entityNotFoundException(InvalidEntityException ex){
        ErrorMessage errorManager = ErrorMessage.builder()
                .message(ex.getMessage())
                .timestamp(new Date())
                .code(Status.BAD_REQUEST.getStatusCode())
                .build();
        return new ResponseEntity<>(errorManager, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {BadRequestAlertException.class})
    ResponseEntity<Object> badRequestAlertException(BadRequestAlertException ex){
        ErrorMessage errorManager = ErrorMessage.builder()
                .message(ex.getMessage())
                .timestamp(new Date())
                .code(Status.BAD_REQUEST.getStatusCode())
                .build();
        return new ResponseEntity<>(errorManager, HttpStatus.BAD_REQUEST);
    }

}
