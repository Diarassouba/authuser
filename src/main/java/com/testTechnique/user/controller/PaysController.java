package com.testTechnique.user.controller;

import com.testTechnique.user.controller.error.BadRequestAlertException;
import com.testTechnique.user.service.PaysServiceImpl;
import com.testTechnique.user.service.dto.PaysDTO;
import com.testTechnique.user.service.dto.UserDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Controller REST  pour gérer les {@link com.testTechnique.user.domain.Pays}.
 */
@RestController
@RequestMapping("/api")
public class PaysController {

    private static final String ENTITY_NAME = "pays";
    private final Logger log = LoggerFactory.getLogger(PaysController.class);
    private final PaysServiceImpl paysService;
    @Value("${app.name}")
    private String applicationName;

    public PaysController(PaysServiceImpl paysService) {
        this.paysService = paysService;
    }

    /**
     * {@code POST  /pays} : crée un nouveau pays.
     *
     * @param paysDTO paysDTO à créer.
     * @return {@link ResponseEntity} avec le statut {@code 201 (Created)} et contenant dans son corps le nouveau paysDTO, ou avec le statut {@code 400 (Bad Request)} si pays à déjà un ID.
     * @throws URISyntaxException en cas d'erreur sur la syntaxe de l'URI.
     */
    @ApiOperation(value = "Save a country", notes = "This method save the country", response = PaysDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK"),
            @ApiResponse(code = 400, message = "Bad Request can't save!!!")})
    @PostMapping("/pays")
    public ResponseEntity<PaysDTO> createPays(@RequestBody PaysDTO paysDTO) throws URISyntaxException {
        log.debug("requête REST pour sauver Pays : {}", paysDTO);
        if (paysDTO.getId() != null) {
            throw new BadRequestAlertException("Une nouvelle pays ne peut pas déjà avoir un ID");
        }
        PaysDTO result = paysService.save(paysDTO);
        return ResponseEntity.created(new URI("/api/pays/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /pays} : Met à jour une pays.
     *
     * @param paysDTO paysDTO à mettre à jour.
     * @return {@link ResponseEntity} avec le statut {@code 200 (OK)} et contenant dans son corps la mise à jour de  paysDTO,
     * ou avec le statut {@code 400 (Bad Request)} si paysDTO n'est pas valide,
     * ou avec le statut {@code 500 (Internal Server Error)} si paysDTO ne peut pas être mis à jour.
     * @throws URISyntaxException en cas d'erreur sur la syntaxe de l'URI.
     */
    @ApiOperation(value = "Update a country", notes = "This method Update the country", response = PaysDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK"),
            @ApiResponse(code = 400, message = "Bad Request can't Update!!!")})
    @PutMapping("/pays")
    public ResponseEntity<PaysDTO> updatePays( @RequestBody PaysDTO paysDTO){
        log.debug("requête REST pour mettre à jour Pays : {}", paysDTO);
        if (paysDTO.getId() == null) {
            throw new BadRequestAlertException("C'est un update id est obligatoire");
        }
        PaysDTO result = paysService.save(paysDTO);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, paysDTO.getId().toString()))
                .body(result);
    }

    /**
     * {@code GET  /pays} : retourne l'ensemble des pays.
     *
     * @return {@link ResponseEntity} avec le statut {@code 200 (OK)} et la liste des pays dans son corps.
     */
    @ApiOperation(value = "Get the list of country", notes = "This method Get the list of country", responseContainer = "List<PaysDTO>")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK")})
    @GetMapping("/pays")
    public ResponseEntity<List<PaysDTO>> getAllPays() {
        log.debug("requête REST pour récupérer une page Pays");
        List<PaysDTO> paysDTOS = paysService.findAll();
       return ResponseEntity.ok().body(paysDTOS);
    }

    /**
     * {@code GET  /pays/:id} : retourne le pays dont l'id est spécifié.
     *
     * @param id l'id du  paysDTO à récupérer.
     * @return {@link ResponseEntity} avec le statut {@code 200 (OK)} et dans son corps paysDTO, ou avec le statut {@code 404 (Not Found)}.
     */
    @ApiOperation(value = "Get the country ", notes = "This method Get a country according to the id", response = UserDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK"),
            @ApiResponse(code = 404, message = "Entity not found !!!") })
    @GetMapping("/pays/{id}")
    public ResponseEntity<PaysDTO> getPays(@PathVariable Long id) {
        log.debug("requête REST pour récupérer  Pays : {}", id);
        return ResponseEntity.ok().body(paysService.findOne(id));
    }


    /**
     * {@code DELETE  /pays/:id} : supprime le pays dont l'id est spécifié.
     *
     * @param id l'id de pays à supprimer.
     * @return {@link ResponseEntity} avec le statut {@code 204 (NO_CONTENT)}.
     */
    @ApiOperation(value = "Delete the country ", notes = "This method delete a country according to the id", response = UserDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK"),
            @ApiResponse(code = 404, message = "Entity not found !!!") })
    @DeleteMapping("/pays/{id}")
    public Boolean deleteUtilisateur(@PathVariable Long id) {
        log.debug("REST request pour supprimer le pays : {}", id);

        return paysService.delete(id);
    }
}

