package com.testTechnique.user.controller;

import com.testTechnique.user.controller.error.BadRequestAlertException;
import com.testTechnique.user.service.UserServiceImpl;
import com.testTechnique.user.service.dto.ResponseUserDTO;
import com.testTechnique.user.service.dto.UserDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Controller REST  pour gérer les {@link com.testTechnique.user.domain.Pays}.
 */
@Api(value = "User resource endpoints")
@RestController
@RequestMapping("/api")
public class UserController {
    private static final String ENTITY_NAME = "user";

    @Value("${app.name}")
    private String applicationName;

    private final Logger log = LoggerFactory.getLogger(UserController.class);

    private final UserServiceImpl userService;

    public UserController(UserServiceImpl userService) {
        this.userService = userService;
    }

    /**
     * {@code POST  /user} : crée un nouveau user.
     *
     * @param userDTO userDTO à créer.
     * @return {@link ResponseEntity} avec le statut {@code 201 (Created)} et contenant dans son corps le nouveau userDTO, ou avec le statut {@code 400 (Bad Request)} si user à déjà un ID.
     * @throws URISyntaxException en cas d'erreur sur la syntaxe de l'URI.
     */
    @ApiOperation(value = "Save a user", notes = "This method save the user", response = ResponseUserDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK"),
            @ApiResponse(code = 400, message = "Bad Request can't save!!!"),
            @ApiResponse(code = 404, message = "Entity country not found !!!") })
    @PostMapping("/user")
    public ResponseEntity<ResponseUserDTO> createUser(@RequestBody UserDTO userDTO) throws URISyntaxException {
        log.debug("requête REST pour sauver user : {}", userDTO);
        if (userDTO.getId() != null) {
            throw new BadRequestAlertException("Une nouvelle user ne peut pas déjà avoir un ID");
        }

        ResponseUserDTO responseUserDTO = userService.save(userDTO);
        return ResponseEntity.created(new URI("/api/user/" + responseUserDTO.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("app-user", true, ENTITY_NAME, responseUserDTO.getId().toString()))
                .body(responseUserDTO);
    }


    /**
     * {@code POST  /user} : crée un nouveau user.
     *
     * @param userDTO userDTO à créer.
     * @return {@link ResponseEntity} avec le statut {@code 201 (Created)} et contenant dans son corps le nouveau userDTO.
     * @throws URISyntaxException en cas d'erreur sur la syntaxe de l'URI.
     */
    @ApiOperation(value = " update a user", notes = "This method update the user", response = ResponseUserDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK"),
            @ApiResponse(code = 400, message = "Bad Request can't save!!!"),
            @ApiResponse(code = 404, message = "Entity country not found !!!") })
    @PutMapping("/user")
    public ResponseEntity<ResponseUserDTO> updateUser(@RequestBody UserDTO userDTO) throws URISyntaxException {
        log.debug("requête REST pour sauver user : {}", userDTO);
        if (userDTO.getId() == null) {
            throw new BadRequestAlertException("C'est un update id est obligatoire");
        }
        ResponseUserDTO responseUserDTO = userService.save(userDTO);
        return ResponseEntity.created(new URI("/api/user/" + responseUserDTO.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("app-user", true, ENTITY_NAME, responseUserDTO.getId().toString()))
                .body(responseUserDTO);
    }

    /**
     * {@code et  /user/{id}} : recuperer un user.
     *
     * @param id id user.
     * @return {@link ResponseEntity} avec le statut {@code 201 (Created)} et contenant dans son corps le nouveau userDTO.
     * @throws URISyntaxException en cas d'erreur sur la syntaxe de l'URI.
     */
    @ApiOperation(value = "Get the user ", notes = "This method Get user according to the id", response = UserDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK"),
            @ApiResponse(code = 404, message = "Entity not found !!!") })
    @GetMapping("/user/{id}")
    public ResponseEntity<UserDTO> getUserById(@PathVariable Long id){
        log.debug("requête REST pour sauver user : {}", id);
        return ResponseEntity.ok().body(userService.findById(id));
    }

    /**
     * {@code GET  /users} : retourne l'ensemble des user.
     *
     * @return {@link ResponseEntity} avec le statut {@code 200 (OK)} et la liste des user dans son corps.
     */
    @ApiOperation(value = "Get the list of user", notes = "This method Get the list of user", responseContainer ="List<UserDTO>")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK")})
    @GetMapping("/users")
    public ResponseEntity<List<UserDTO>> getAllAUser() {
        log.debug("requête REST pour récupérer tout les user");
        List<UserDTO> userDTOS = userService.findAll();
        return ResponseEntity.ok().body(userDTOS);
    }

}
