package com.testTechnique.user.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig implements WebMvcConfigurer {

    @Bean
    Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(
                        new ApiInfoBuilder()
                                .description("Management user API documentation ")
                                .title("Management user REST API")
                                .build()
                )
                .groupName("Rest API v1")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.testTechnique.user"))
                .paths(PathSelectors.ant("/api/**"))
                .build();
    }
}
