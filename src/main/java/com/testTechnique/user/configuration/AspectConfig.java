package com.testTechnique.user.configuration;

import com.testTechnique.user.controller.error.BadRequestAlertException;
import com.testTechnique.user.service.error.EntityNotFoundException;
import com.testTechnique.user.service.error.InvalidEntityException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.*;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.server.ResponseStatusException;

@Configuration
@Aspect
public class AspectConfig {

    private Logger log= LoggerFactory.getLogger(AspectConfig.class);

    @Before(value = "execution(* com.testTechnique.user.controleur.*.*(..))")
    public void logStatementBefore(JoinPoint joinPoint) {
        log.info("Executing {}",joinPoint);
    }

    @After(value = "execution(* com.testTechnique.user.controleur.*.*(..))")
    public void logStatementAfter(JoinPoint joinPoint) {
        log.info("Complete exceution of {}",joinPoint);
    }

    @Around(value = "execution(* com.testTechnique.user.controleur.*.*(..))")
    public Object timeTracker(ProceedingJoinPoint joinPoint) throws Throwable {

        long stratTime=System.currentTimeMillis();

        try {
            Object obj=joinPoint.proceed();
            long timeTaken=System.currentTimeMillis()-stratTime;
            log.info("Time taken by {} is {}",joinPoint,timeTaken);
            return obj;
        }
        catch(BadRequestAlertException e ) {
            log.info(" TaskException StatusCode {}",e.getHttpStatus().value());
            log.info("TaskException Message {}",e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }catch (EntityNotFoundException e){
            log.info(" TaskException StatusCode {}",e.getHttpStatus().value());
            log.info("TaskException Message {}",e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }catch (InvalidEntityException e){
            log.info(" TaskException StatusCode {}",e.getHttpStatus().value());
            log.info("TaskException Message {}",e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }
    }
}
