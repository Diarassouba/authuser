SET NAMES utf8;
--SET time_zone = '+00:00';
/* Vérification des contraintes d''intégrité désactivée */
SET foreign_key_checks = 0;
--SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;
CREATE SCHEMA testdb AUTHORIZATION SA;
--CREATE DATABASE `testdb` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `testdb`;
CREATE TABLE IF NOT EXISTS pays(id INT PRIMARY KEY, age_majorite INT, code VARCHAR(255), nom VARCHAR(255));


CREATE TABLE IF NOT EXISTS utilisateur(
id INT PRIMARY KEY,
name VARCHAR(255),
date_naissance date,
pays_id      INT,
 foreign key (pays_id) references pays(pays_id)
 );
-- ALTER TABLE utilisateur
--    ADD FOREIGN KEY (pays_id)
--    REFERENCES pays(pays_id);

/* Vérification des contraintes d''intégrité activée */
SET foreign_key_checks = 1;

-- Add the initial structure if necessary
;
