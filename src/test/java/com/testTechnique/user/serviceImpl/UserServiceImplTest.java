package com.testTechnique.user.serviceImpl;


import com.testTechnique.user.UserApplication;
import com.testTechnique.user.domain.Pays;
import com.testTechnique.user.repository.PaysRepository;
import com.testTechnique.user.repository.UserRepository;
import com.testTechnique.user.service.UserServiceImpl;
import com.testTechnique.user.service.dto.ResponseUserDTO;
import com.testTechnique.user.service.dto.UserDTO;
import com.testTechnique.user.service.error.InvalidEntityException;
import com.testTechnique.user.service.mapper.ResponseMapper;
import com.testTechnique.user.service.mapper.UserMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(classes = UserApplication.class)
public class UserServiceImplTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    PaysRepository paysRepository;

    @Autowired
    UserMapper userMapper;

    @Autowired
    ResponseMapper responseMapper;

    UserServiceImpl userService;

    UserDTO userDTO;
    Pays pays;
    
    @BeforeEach
    public void init() {
       userRepository.deleteAll();

    }

    @Test
    public void shouldSaveUser( ){
        userService = new UserServiceImpl(userMapper,responseMapper,paysRepository,userRepository);

        paysRepository.save(initPays());
        ResponseUserDTO responseUserDTO =   userService.save(initUserDTO());
        assertEquals(responseUserDTO.getId(),3);
        assertEquals(responseUserDTO.getName(),"Marie");
    }

    @Test
    public void shouldSaveUser_WhenNamePaysEqualsFranceAndAge10() {
        userService = new UserServiceImpl(userMapper,responseMapper,paysRepository,userRepository);
        paysRepository.save(new Pays(1L,"FR",20,"France" ));
        UserDTO userDTO = initUserDTO();
        userDTO.setDateNaissance(LocalDate.now().minusYears(10));
       userDTO.setPays(new Pays(1L,"FR",20,"France" ));
        assertThrows(InvalidEntityException.class, () -> userService.save(userDTO));
    }


    @Test
    public void shouldfindById() {
        userService = new UserServiceImpl(userMapper,responseMapper,paysRepository,userRepository);
        save();
        assertEquals(userService.findById(1L), userDTO());
    }

    @Test
    public void shouldfindAll() {
        userService = new UserServiceImpl(userMapper,responseMapper,paysRepository,userRepository);
        save();
        assertEquals(userService.findAll().size(),2);
    }


    Pays initPays(){
        pays = new Pays(1L,"SN",20,"Sénégal " );
        return pays;
    }

    UserDTO initUserDTO(){
        userDTO = new UserDTO(null,
                "Marie",
                LocalDate.now().minusYears(25),
                initPays(),
                "772364593",
                "Femme");
        return userDTO;
    }
    UserDTO userDTO(){
        userDTO = new UserDTO(1L,
                "Marie",
                LocalDate.now().minusYears(25),
                initPays(),
                "772364593",
                "Femme");
        return userDTO;
    }

    public void save(){
        UserDTO userDTO = initUserDTO();
        userDTO.setName("Yves");
        userDTO.setSexe("homme");
        paysRepository.save(initPays());
        userService.save(initUserDTO());
        userService.save(userDTO);
    }
}
