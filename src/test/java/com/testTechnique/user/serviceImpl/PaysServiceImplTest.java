package com.testTechnique.user.serviceImpl;

import com.testTechnique.user.UserApplication;
import com.testTechnique.user.domain.Pays;
import com.testTechnique.user.repository.PaysRepository;
import com.testTechnique.user.service.PaysServiceImpl;
import com.testTechnique.user.service.dto.PaysDTO;
import com.testTechnique.user.service.mapper.PaysMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = UserApplication.class)
public class PaysServiceImplTest {

    @Autowired
    private  PaysMapper paysMapper;

    @Autowired
    private  PaysRepository paysRepository;

    private PaysServiceImpl paysService;

    Pays pays;

    PaysDTO paysDTO;

    Pays initPays(){
        pays = new Pays(null,"SN",20,"Sénégal" );
        return pays;
    }
    PaysDTO initPaysDTO(){
        paysDTO = new PaysDTO(1L,"SN",20,"Sénégal" );
        return paysDTO;
    }

    @Test
    public void shouldSavePays( ){
        paysService = new PaysServiceImpl(paysMapper, paysRepository);
        paysRepository.save(initPays());
        PaysDTO paysDTO =   paysService.save(initPaysDTO());
        assertEquals(paysDTO.getId(),1);
        assertEquals(paysDTO.getNom(),"Sénégal");
    }

    @Test
    public void shouldfindAll() {
        paysService = new PaysServiceImpl(paysMapper, paysRepository);
        save();
        assertEquals(paysService.findAll().size(),3);
    }

    @Test
    public void shouldfindOne() {
        paysService = new PaysServiceImpl(paysMapper, paysRepository);
        paysRepository.save(initPays());
        assertEquals(paysService.findOne(1L).getNom(),"Cote d'Ivoire");
    }

    public void save(){
        PaysDTO paysDTO = initPaysDTO();
        paysDTO.setNom("Cote d'Ivoire");
        paysDTO.setCode("CI");
        paysDTO.setAgeMajorite(18);
        paysService.save(paysDTO);
        paysRepository.save(initPays());
    }
}
