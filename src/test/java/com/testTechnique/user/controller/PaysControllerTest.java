package com.testTechnique.user.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.testTechnique.user.UserApplication;
import com.testTechnique.user.service.dto.PaysDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest(classes = UserApplication.class)
public class PaysControllerTest {
    protected static final String API = "/api/";
    private static final String END_POINT = API + "pays";
    private static final Long ID =1L;
    private static final String SEPARATEUR = "/";

    protected static final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    MockHttpServletResponse mockMvcResponse;

    PaysDTO paysDTO;
    PaysDTO initPaysDTO(){
        paysDTO = new PaysDTO(null,"SN",20,"Sénégal" );
        return paysDTO;
    }

    @Test
    void shouldCreatePays() throws Exception {
    // Given
        String jsonRequest = mapper.writeValueAsString(initPaysDTO());
    // When
        String paysResponse =  mockMvc.perform(post(
                    END_POINT).contentType(MediaType.APPLICATION_JSON).content(jsonRequest)
            ).andExpect(status().isCreated()).andReturn().getResponse().getContentAsString();
        PaysDTO nouvellePays = mapper.readValue(paysResponse, PaysDTO.class);
    // Then
        assertEquals(initPaysDTO().getCode(), nouvellePays.getCode());
    }

    @Test
    void shouldCreatePays_WhenIdPaysIsNotNull() throws Exception {
        // Given
        PaysDTO paysDTO = initPaysDTO();
        paysDTO.setId(1L);
        String jsonRequest = mapper.writeValueAsString(paysDTO);
        // When
        ResultActions paysResponse =  mockMvc.perform(post(
                END_POINT).contentType(MediaType.APPLICATION_JSON).content(jsonRequest)
        );
        // Then
        paysResponse.andExpect(status().isBadRequest());
    }

    @Test
    void shouldUpdatePays() throws Exception {
        // Given
        PaysDTO paysDTO = initPaysDTO();
        paysDTO.setId(1L);
        String jsonRequest = mapper.writeValueAsString(paysDTO);
        // When
        String paysResponse =  mockMvc.perform(put(
                END_POINT).contentType(MediaType.APPLICATION_JSON).content(jsonRequest)
        ).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        PaysDTO nouvellePays = mapper.readValue(paysResponse, PaysDTO.class);
        // Then
        assertEquals(initPaysDTO().getCode(), nouvellePays.getCode());
    }

    @Test
    void shouldUpdatePays_WhenIdPaysIsNull() throws Exception {
        // Given
        String jsonRequest = mapper.writeValueAsString(initPaysDTO());
        // When
        ResultActions paysResponse =  mockMvc.perform(put(
                END_POINT).contentType(MediaType.APPLICATION_JSON).content(jsonRequest)
        );
        // Then
        paysResponse.andExpect(status().isBadRequest());
    }

    @Test
    void getAdresseObtenirUneAdresseParId() throws Exception {

        String reponse = mockMvc.perform(MockMvcRequestBuilders.get(END_POINT + SEPARATEUR + ID))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PaysDTO getPays = mapper.readValue(reponse, PaysDTO.class);

        assertEquals(initPaysDTO().getAgeMajorite(), getPays.getAgeMajorite());
    }
}
